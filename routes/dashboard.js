var express = require('express');
var router = express.Router();

router.get('/', function(req, res, next) {
    console.log(req.user);

    res.render('dashboard', {layout: 'main_layout', activeDashboard: true, title: 'Dashboard', isSignedIn: req.user !== undefined});
});

module.exports = router;