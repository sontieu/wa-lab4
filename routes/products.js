/**
 * Created by ttson on 5/1/2017.
 */

var express = require('express');
var productController = require('../controllers/productController');
var router = express.Router();
var multer  = require('multer');
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'public/uploads/')
    },
    filename: function (req, file, cb) {
        cb(null, file.originalname)
    }
});
var upload = multer({ storage: storage });

function checkSignedIn(req, res, next) {
    if (req.user)
        next();
    else
        res.redirect('/login');
}

var itemRouter = express.Router({mergeParams: true});
var deleteRouter =  express.Router({mergeParams: true});
router.use('/delete', deleteRouter);
router.use('/item', itemRouter);

router.get('/', checkSignedIn, productController.product_list);

itemRouter.get('/:id',checkSignedIn, productController.product_item);
itemRouter.post('/:id', checkSignedIn, productController.product_update);

deleteRouter.post('/', checkSignedIn, productController.product_delete);

router.get('/new', checkSignedIn, function(req, res, next) {
    res.render('product_item', {layout: 'main_layout', title: 'Add new product', activeProduct: true, activeAddProduct: true, isSignedIn: req.user !== undefined});
});

router.post('/new', upload.single('item_thumbnail'), productController.product_insert);



module.exports = router;
