var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var ProductSchema = Schema(
    {
        sku: {type: String},
        model: {type: String},
        title: {type: String},
        size: {type: String},
        thumbnail: {type: String}
    }
);

module.exports = mongoose.model('Product', ProductSchema);