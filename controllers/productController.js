var Product = require('../models/product');
var uploadFolder = '/uploads/';


var product_insert = function (req, res, next) {
    var productInst = new Product({
        sku: req.body.item_sku,
        model: req.body.item_model,
        title: req.body.item_title,
        size: req.body.item_size,
        thumbnail: uploadFolder + req.file.filename
    });

    productInst.save(function (err) {
        if (err) {
            console.log(err);
            return;
        }

        res.render('product_item', {
            layout: 'main_layout',
            title: 'Add new product',
            activeProduct: true,
            activeAddProduct: true,
            isSignedIn: req.user !== undefined
        })
    });
};
var product_list = function(req, res, next) {
    Product.find({}, function (err, products) {
        if (err) {
            console.log(err);
            return;
        }

        res.render('products', {
            layout: 'main_layout',
            title: 'All Products',
            activeProduct: true,
            activeAllProducts: true,
            items: products,
            isSignedIn: req.user !== undefined
        });
    })
};
var product_item = function(req, res, next) {
    Product.findById(req.params.id, function(error, product) {
        if (error) {
            console.log(error);
            return;
        }
        res.render('product_item', {layout: 'main_layout', title: 'Edit: ' + product.title, isEdit: true, activeProduct: true, error: '' , item: product,
            isSignedIn: req.user !== undefined});
    });
};
var product_update = function (req, res, next) {

    Product.findByIdAndUpdate(req.params.id, {
        sku: req.body.item_sku,
        model: req.body.item_model,
        title: req.body.item_title,
        size: req.body.item_size,
        thumbnail: req.body.thumbnail
    }, function (err, product) {
        if (err) {
            console.log(err);
            return;
        }

        res.render('product_item', {layout: 'main_layout', title: 'Edit: ' + product.title, isEdit: true, activeProduct: true, error: '' , item: product,
            isSignedIn: req.user !== undefined});
    });
};
var product_delete = function (req, res, next) {
    console.log(req.params.id);

    Product.findByIdAndRemove(req.body.id, function (err, product) {
        if (err) {
            console.log(err);
            return;
        }

        Product.find({}, function (err, products) {
            if (err) {
                console.log(err);
                return;
            }

            res.redirect('/products');
        });
    });


};

exports.product_insert = product_insert;
exports.product_list = product_list;
exports.product_item = product_item;
exports.product_update = product_update;
exports.product_delete = product_delete;